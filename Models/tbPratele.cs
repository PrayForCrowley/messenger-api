﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class tbPratele
    {
        public int ID { get; set; }

        [ForeignKey("user1")]
        public int ID_Uzivatele { get; set; }

        [ForeignKey("user2")]
        public int ID_Pritele { get; set; }

        public DateTime Datum_Pratelstvi { get; set; }

        public bool Potvrzeno { get; set; }

        public tbUzivatel user1 { get; set; }
        public tbUzivatel user2 { get; set; }
    }
}