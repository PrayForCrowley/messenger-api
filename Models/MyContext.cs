﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    [DbConfigurationType(typeof(MySql.Data.EntityFramework.MySqlEFConfiguration))]
    public class MyContext : DbContext
    {
        public DbSet<tbUzivatel> Uzivatel { get; set; }
        public DbSet<tbZpravy> Zprava { get; set; }
        public DbSet<tbSkupinovyChat> Chat { get; set; }
        public DbSet<tbUcastniciChatu> Chat_Ucastnici { get; set; }
        public DbSet<tbPratele> Pratele { get; set; }
        public DbSet<tbPrecteno> Message_Stav { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }

}