﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class tbPrateleRepository
    {
        private MyContext context = new MyContext();

        public List<tbPratele> FindAll()
        {
            return this.context.Pratele.ToList();
        }

        public tbPratele FindById(int id)
        {
            //return this.context.People.Where(x => x.Id == id).FirstOrDefault();
            return this.context.Pratele.Find(id);
        }

        public void Create(tbPratele friend_list)
        {
            this.context.Pratele.Add(friend_list);
            this.context.SaveChanges();
        }

        public void Update(tbPratele friend_list)
        {
            /*Person current = this.FindById(person.Id);

            current.Name = person.Name;
            current.Surname = person.Surname;
            current.Age = person.Age;

            this.context.SaveChanges();*/

            this.context.Entry(friend_list).State = System.Data.Entity.EntityState.Modified;
            this.context.SaveChanges();
        }

        public void Delete(tbPratele friend_list)
        {
            this.context.Pratele.Remove(friend_list);
            this.context.SaveChanges();
        }
    }
}