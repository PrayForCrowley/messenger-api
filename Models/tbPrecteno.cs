﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class tbPrecteno
    {
        public int ID { get; set; }

        [ForeignKey("message")]
        public int ID_Zpravy { get; set; }

        [ForeignKey("user")]
        public int ID_Uzivatele { get; set; }

        public DateTime Datum_Precteni { get; set; }

        public DateTime Datum_Odeslani { get; set; }

        public System.DateTime Datum_Prijeti { get; set; }

        public tbZpravy message { get; set; }
        public tbUzivatel user { get; set; }
    }
}