﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class tbUzivatel
    {
        public int ID { get; set; }

        public string Email { get; set; }

        public string Prezdivka { get; set; }

        public string Heslo { get; set; }

        public string Foto { get; set; }

        public DateTime Datum_Vytvoreni { get; set; }

        public DateTime Datum_Odhlaseni { get; set; }

    }
}