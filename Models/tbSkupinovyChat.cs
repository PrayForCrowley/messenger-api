﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class tbSkupinovyChat
    {
        public int ID { get; set; }

        public int? Barva { get; set; }

        public string Nazev_Chatu { get; set; }
    }
}