﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class tbUzivatelRepository
    {
        private MyContext context = new MyContext();

        public List<tbUzivatel> FindAll()
        {
            return this.context.Uzivatel.ToList();
        }

        public tbUzivatel FindById(int id)
        {
            //return this.context.People.Where(x => x.Id == id).FirstOrDefault();
            return this.context.Uzivatel.Find(id);
        }

        public void Create(tbUzivatel user)
        {
            this.context.Uzivatel.Add(user);
            this.context.SaveChanges();
        }

        public void Update(tbUzivatel user)
        {
            /*Person current = this.FindById(person.Id);

            current.Name = person.Name;
            current.Surname = person.Surname;
            current.Age = person.Age;

            this.context.SaveChanges();*/

            this.context.Entry(user).State = System.Data.Entity.EntityState.Modified;
            this.context.SaveChanges();
        }

        public void Delete(tbUzivatel user)
        {
            this.context.Uzivatel.Remove(user);
            this.context.SaveChanges();
        }
    }
}