﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class tbSkupinovyChatRepository
    {
        private MyContext context = new MyContext();

        public List<tbSkupinovyChat> FindAll()
        {
            return this.context.Chat.ToList();
        }

        public tbSkupinovyChat FindById(int id)
        {
            //return this.context.People.Where(x => x.Id == id).FirstOrDefault();
            return this.context.Chat.Find(id);
        }

        public void Create(tbSkupinovyChat chat)
        {
            this.context.Chat.Add(chat);
            this.context.SaveChanges();
        }

        public void Update(tbSkupinovyChat chat)
        {
            /*Person current = this.FindById(person.Id);

            current.Name = person.Name;
            current.Surname = person.Surname;
            current.Age = person.Age;

            this.context.SaveChanges();*/

            this.context.Entry(chat).State = System.Data.Entity.EntityState.Modified;
            this.context.SaveChanges();
        }

        public void Delete(tbSkupinovyChat chat)
        {
            this.context.Chat.Remove(chat);
            this.context.SaveChanges();
        }
    }
}