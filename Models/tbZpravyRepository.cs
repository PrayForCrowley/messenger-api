﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class tbZpravyRepository
    {
        private MyContext context = new MyContext();

        public List<tbZpravy> FindAll()
        {
            return this.context.Zprava.ToList();
        }

        public tbZpravy FindById(int id)
        {
            //return this.context.People.Where(x => x.Id == id).FirstOrDefault();
            return this.context.Zprava.Find(id);
        }

        public void Create(tbZpravy message)
        {
            this.context.Zprava.Add(message);
            this.context.SaveChanges();
        }

        public void Update(tbZpravy message)
        {
            /*Person current = this.FindById(person.Id);

            current.Name = person.Name;
            current.Surname = person.Surname;
            current.Age = person.Age;

            this.context.SaveChanges();*/

            this.context.Entry(message).State = System.Data.Entity.EntityState.Modified;
            this.context.SaveChanges();
        }

        public void Delete(tbZpravy message)
        {
            this.context.Zprava.Remove(message);
            this.context.SaveChanges();
        }
    }
}