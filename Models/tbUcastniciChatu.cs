﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

using System.Web;

namespace WebAPI.Models
{
    public class tbUcastniciChatu
    {
        public int ID { get; set; }

        [ForeignKey("chat")]
        public int ID_Chatu { get; set; }

        [ForeignKey("user")]
        public int ID_Uzivatele { get; set; }

        public string Role { get; set; }

        public string Prezdivka { get; set; }

        public tbSkupinovyChat chat { get; set; }
        public tbUzivatel user { get; set; }
    }
}