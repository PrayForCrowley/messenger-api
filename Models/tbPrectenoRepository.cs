﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class tbPrectenoRepository
    {
        private MyContext context = new MyContext();

        public List<tbPrecteno> FindAll()
        {
            return this.context.Message_Stav.ToList();
        }

        public tbPrecteno FindById(int id)
        {
            //return this.context.People.Where(x => x.Id == id).FirstOrDefault();
            return this.context.Message_Stav.Find(id);
        }

        public void Create(tbPrecteno message_state)
        {
            this.context.Message_Stav.Add(message_state);
            this.context.SaveChanges();
        }

        public void Update(tbPrecteno message_state)
        {
            /*Person current = this.FindById(person.Id);

            current.Name = person.Name;
            current.Surname = person.Surname;
            current.Age = person.Age;

            this.context.SaveChanges();*/

            this.context.Entry(message_state).State = System.Data.Entity.EntityState.Modified;
            this.context.SaveChanges();
        }

        public void Delete(tbPrecteno message_state)
        {
            this.context.Message_Stav.Remove(message_state);
            this.context.SaveChanges();
        }
    }
}