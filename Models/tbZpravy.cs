﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class tbZpravy
    {
        //[ForeignKey("chats")]


        public int ID { get; set; }

        public string Jmeno { get; set; }

        public string Obsah_Zpravy { get; set; }

        public DateTime Datum_Odeslani { get; set; }

        //public int Zprava_Od { get; set; }

        //public string Soubor { get; set; }

        //public DateTime Datum_Upravy { get; set; }

        //public tbSkupinovyChat chats { get; set; }

    }
}