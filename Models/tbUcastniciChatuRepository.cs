﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class tbUcastniciChatuRepository
    {
        private MyContext context = new MyContext();

        public List<tbUcastniciChatu> FindAll()
        {
            return this.context.Chat_Ucastnici.ToList();
        }

        public tbUcastniciChatu FindById(int id)
        {
            //return this.context.People.Where(x => x.Id == id).FirstOrDefault();
            return this.context.Chat_Ucastnici.Find(id);
        }

        public void Create(tbUcastniciChatu chat_participants)
        {
            this.context.Chat_Ucastnici.Add(chat_participants);
            this.context.SaveChanges();
        }

        public void Update(tbUcastniciChatu chat_participants)
        {
            /*Person current = this.FindById(person.Id);

            current.Name = person.Name;
            current.Surname = person.Surname;
            current.Age = person.Age;

            this.context.SaveChanges();*/

            this.context.Entry(chat_participants).State = System.Data.Entity.EntityState.Modified;
            this.context.SaveChanges();
        }

        public void Delete(tbUcastniciChatu chat_participants)
        {
            this.context.Chat_Ucastnici.Remove(chat_participants);
            this.context.SaveChanges();
        }
    }
}