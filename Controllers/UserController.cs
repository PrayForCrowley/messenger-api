﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserController : ApiController
    {
        private tbUzivatelRepository repository = new tbUzivatelRepository();

        // GET: api/People
        [HttpGet]
        public IEnumerable<tbUzivatel> Get()
        {
            return this.repository.FindAll();
        }

        // GET: api/People/5
        [HttpGet]
        public tbUzivatel Get(int id)
        {
            return this.repository.FindById(id);
        }

        // POST: api/People
        [HttpPost]
        public void Post([FromBody]tbUzivatel value)
        {
            this.repository.Create(value);
        }

        // PUT: api/People/5
        [HttpPut]
        public void Put(int id, [FromBody]tbUzivatel value)
        {
            value.ID = id;
            this.repository.Update(value);
        }

        // DELETE: api/People/5
        [HttpDelete]
        public void Delete(int id)
        {
            tbUzivatel user = this.repository.FindById(id);
            this.repository.Delete(user);
        }
    }
}
