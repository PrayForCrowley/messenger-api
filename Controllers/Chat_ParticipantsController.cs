﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class Chat_ParticipantsController : ApiController
    {
        private tbUcastniciChatuRepository repository = new tbUcastniciChatuRepository();

        // GET: api/Chat_Participants
        public IEnumerable<tbUcastniciChatu> Get()
        {
            return this.repository.FindAll();
        }

        // GET: api/Chat_Participants/5
        [HttpGet]
        public tbUcastniciChatu Get(int id)
        {
            return this.repository.FindById(id);
        }

        // POST: api/Chat_Participants
        [HttpPost]
        public void Post([FromBody]tbUcastniciChatu value)
        {
            this.repository.Create(value);
        }

        // PUT: api/Chat_Participants/5
        [HttpPut]
        public void Put(int id, [FromBody]tbUcastniciChatu value)
        {
            value.ID_Chatu = id;
            this.repository.Update(value);
        }

        // DELETE: api/Chat_Participants/5
        [HttpDelete]
        public void Delete(int id)
        {
            tbUcastniciChatu chat_participants = this.repository.FindById(id);
            this.repository.Delete(chat_participants);
        }
    }
}
