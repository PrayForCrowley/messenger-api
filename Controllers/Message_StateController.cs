﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class Message_StateController : ApiController
    {
        private tbPrectenoRepository repository = new tbPrectenoRepository();

        // GET: api/Chat_Participants
        public IEnumerable<tbPrecteno> Get()
        {
            return this.repository.FindAll();
        }

        // GET: api/Chat_Participants/5
        [HttpGet]
        public tbPrecteno Get(int id)
        {
            return this.repository.FindById(id);
        }

        // POST: api/Chat_Participants
        [HttpPost]
        public void Post([FromBody]tbPrecteno value)
        {
            this.repository.Create(value);
        }

        // PUT: api/Chat_Participants/5
        [HttpPut]
        public void Put(int id, [FromBody]tbPrecteno value)
        {
            value.ID_Zpravy = id;
            this.repository.Update(value);
        }

        // DELETE: api/Chat_Participants/5
        [HttpDelete]
        public void Delete(int id)
        {
            tbPrecteno message_state = this.repository.FindById(id);
            this.repository.Delete(message_state);
        }
    }
}
