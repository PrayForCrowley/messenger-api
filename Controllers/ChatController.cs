﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class ChatController : ApiController
    {
        private tbSkupinovyChatRepository repository = new tbSkupinovyChatRepository();

        // GET: api/Chat
        public IEnumerable<tbSkupinovyChat> Get()
        {
            return this.repository.FindAll();
        }

        // GET: api/Chat/5
        [HttpGet]
        public tbSkupinovyChat Get(int id)
        {
            return this.repository.FindById(id);
        }

        // POST: api/Chat
        [HttpPost]
        public void Post([FromBody]tbSkupinovyChat value)
        {
            this.repository.Create(value);
        }

        // PUT: api/Chat/5
        [HttpPut]
        public void Put(int id, [FromBody]tbSkupinovyChat value)
        {
            value.ID = id;
            this.repository.Update(value);
        }

        // DELETE: api/Chat/5
        [HttpDelete]
        public void Delete(int id)
        {
            tbSkupinovyChat chat = this.repository.FindById(id);
            this.repository.Delete(chat);
        }
    }
}
