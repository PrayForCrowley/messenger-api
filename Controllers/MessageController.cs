﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class MessageController : ApiController
    {

        private tbZpravyRepository repository = new tbZpravyRepository();

        // GET: api/Message
        [HttpGet]
        public IEnumerable<tbZpravy> Get()
        {
            return this.repository.FindAll();
        }

        // GET: api/Message/5
        [HttpGet]
        public tbZpravy Get(int id)
        {
            return this.repository.FindById(id);
        }

        // POST: api/Message
        [HttpPost]
        public void Post([FromBody]tbZpravy value)
        {
            this.repository.Create(value);
        }

        // PUT: api/Message/5
        [HttpPut]
        public void Put(int id, [FromBody]tbZpravy value)
        {
            value.ID = id;
            this.repository.Update(value);
        }

        // DELETE: api/Message/5
        [HttpDelete]
        public void Delete(int id)
        {
            tbZpravy message = this.repository.FindById(id);
            this.repository.Delete(message);
        }
    }
}
