﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class Friend_ListController : ApiController
    {
        private tbPrateleRepository repository = new tbPrateleRepository();

        // GET: api/Chat_Participants
        public IEnumerable<tbPratele> Get()
        {
            return this.repository.FindAll();
        }

        // GET: api/Chat_Participants/5
        [HttpGet]
        public tbPratele Get(int id)
        {
            return this.repository.FindById(id);
        }

        // POST: api/Chat_Participants
        [HttpPost]
        public void Post([FromBody]tbPratele value)
        {
            this.repository.Create(value);
        }

        // PUT: api/Chat_Participants/5
        [HttpPut]
        public void Put(int id, [FromBody]tbPratele value)
        {
            value.ID = id;
            this.repository.Update(value);
        }

        // DELETE: api/Chat_Participants/5
        [HttpDelete]
        public void Delete(int id)
        {
            tbPratele friend_list = this.repository.FindById(id);
            this.repository.Delete(friend_list);
        }
    }
}
